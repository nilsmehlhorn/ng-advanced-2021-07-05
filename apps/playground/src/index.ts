import { connectable, Observable, Subject } from 'rxjs';

console.log('Start');

const timer$ = new Observable((observer) => {
  console.log('Producer');
  observer.next(1);
  observer.next(2);
  observer.next(3);
  observer.error('Error');
});

const hot$ = connectable(timer$, {
  connector: () => new Subject(), // this is resetOnDisconnect
  resetOnDisconnect: true
});

hot$.subscribe({
  next: (value) => console.log(`A${value}`),
  error: () => console.log('Error in A')
});

hot$.connect();

setTimeout(() => {
  hot$.subscribe({
    next: (value) => console.log(`B${value}`),
    error: () => console.log('Error in B')
  });
}, 3000);
