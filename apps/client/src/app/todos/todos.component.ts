import { Component, OnInit } from '@angular/core';
import { EMPTY, merge, Observable, of, Subject } from 'rxjs';
import { first, map, mapTo, skip, withLatestFrom } from 'rxjs/operators';
import { Todo } from './models';
import { TodoService } from './todo.service';

@Component({
  selector: 'dos-todos',
  templateUrl: './todos.component.html'
})
export class TodosComponent implements OnInit {
  todos$: Observable<Todo[]>;
  private todosSource$ = this.todosService.loadFrequently();
  private todosInitial$: Observable<Todo[]>;
  private todosMostRecent$: Observable<Todo[]>;

  update$$ = new Subject<void>();
  showReload$: Observable<boolean> = EMPTY;
  private show$: Observable<boolean>;
  private hide$: Observable<boolean>;

  constructor(private todosService: TodoService) {}

  ngOnInit(): void {
    this.todosMostRecent$ = this.update$$.pipe(
      withLatestFrom(this.todosSource$),
      map(([signal, todos]) => todos)
    );
    this.todosInitial$ = this.todosSource$.pipe(first());
    this.todos$ = merge(this.todosInitial$, this.todosMostRecent$);

    this.show$ = this.todosSource$.pipe(skip(1), mapTo(true));
    this.hide$ = this.update$$.pipe(mapTo(false));
    this.showReload$ = merge(this.show$, this.hide$);
  }

  completeOrIncompleteTodo(todoForUpdate: Todo) {
    /*
     * Note in order to keep the code clean for the workshop we did not
     * handle the following subscription.
     * Normally you want to unsubscribe.
     *
     * We just want to focus you on RxJS.
     */
    this.todosService.completeOrIncomplete(todoForUpdate).subscribe();
  }
}
