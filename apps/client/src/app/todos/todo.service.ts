import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { defer, Observable, timer } from 'rxjs';
import {
  delay,
  exhaustMap,
  map,
  retry,
  retryWhen,
  share,
  tap
} from 'rxjs/operators';
import { Toolbelt } from './internals';
import { Todo, TodoApi } from './models';
import { TodoSettings } from './todo-settings.service';

const todosUrl = 'http://localhost:3333/api';

@Injectable()
export class TodoService {
  constructor(
    private http: HttpClient,
    private toolbelt: Toolbelt,
    private settings: TodoSettings
  ) {}

  loadFrequently(): Observable<Todo[]> {
    // TODO: Introduce error handled, configured, recurring, all-mighty stream
    return defer(() => {
      console.log('Creating timer');
      return timer(0, 5000);
    }).pipe(
      exhaustMap(() =>
        this.query().pipe(
          retryWhen((errors) => {
            console.log('Error occurred');
            return errors.pipe(
              delay(6000),
              tap(() => console.log('Retrying now'))
            );
          })
        )
      ),
      tap({ error: () => this.toolbelt.offerHardReload() }),
      share()
    );
  }

  // TODO: Fix the return type of this method
  private query(): Observable<Todo[]> {
    return this.http
      .get<TodoApi[]>(`${todosUrl}`)
      .pipe(
        map((todosFromApi) =>
          todosFromApi.map((todoFromApi) => this.toolbelt.toTodo(todoFromApi))
        )
      );
  }

  create(todo: Todo): Observable<TodoApi> {
    return this.http.post<TodoApi>(todosUrl, todo);
  }

  remove(todoForRemoval: TodoApi): Observable<Todo> {
    return this.http
      .delete<TodoApi>(`${todosUrl}/${todoForRemoval.id}`)
      .pipe(map((todo) => this.toolbelt.toTodo(todo)));
  }

  completeOrIncomplete(todoForUpdate: Todo): Observable<Todo> {
    const updatedTodo = this.toggleTodoState(todoForUpdate);
    return this.http
      .put<TodoApi>(
        `${todosUrl}/${todoForUpdate.id}`,
        this.toolbelt.toTodoApi(updatedTodo)
      )
      .pipe(map((todo) => this.toolbelt.toTodo(todo)));
  }

  private toggleTodoState(todoForUpdate: Todo): Todo {
    todoForUpdate.isDone = todoForUpdate.isDone ? false : true;
    return todoForUpdate;
  }
}
